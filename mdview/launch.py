import ctypes
import os
import sys
from multiprocessing import current_process, set_start_method
from os import listdir, makedirs, name, path, remove
from time import time
sys.path = sorted(sys.path)

import matplotlib

matplotlib.use("Qt5Agg")

if current_process().name == "MainProcess":
    set_start_method("spawn")
    import sys
    from PyQt5 import QtCore, QtGui, QtWidgets


def initApp():
    """Create application."""
    QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps)
    QtWidgets.QApplication.setAttribute(
        QtCore.Qt.AA_CompressHighFrequencyEvents
    )
    app = QtWidgets.QApplication(sys.argv)

    if sys.platform == "win32":
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
            "mdview")

    app.setApplicationName("MDView")
    app.setApplicationDisplayName("MDView")
    return app


def launchMain():
    """Create application and MainWindow."""
    global app, mainWindow

    app = initApp()

    from mdview.mainwindow import MainWindow
    mainWindow = MainWindow()
    mainWindow.showMaximized()
    return app.exec_()


def exportAll():
    app = initApp()

    dirname = os.path.abspath(
        os.path.normpath(os.path.splitext(sys.argv[1])[0])) + "-data"
    if not os.path.isdir(dirname):
        try:
            os.mkdir(dirname)
        except Exception as e:
            print(e)
            exit()

    from mdview.mainwindow import MainWindow
    mainWindow = MainWindow()
    mainWindow.exportAll(dirname)


def saveImages():
    app = initApp()

    from mdview.mainwindow import MainWindow
    mainWindow = MainWindow()
    mainWindow.saveImages(sys.argv[3])


if __name__ == "__main__":
    sys.exit(launchMain())
