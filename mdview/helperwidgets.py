from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from mdview.signal import Signal


class PathLineEdit(QLineEdit):

    enterPressed = Signal()

    def keyPressEvent(self, event: QKeyEvent):
        if event.key() == Qt.Key_Return:
            self.enterPressed.emit()
        super().keyPressEvent(event)
