import os
import sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from mdview.mainview import MainView
from mdview.util import convertStrToIndex


class MainWindow(QMainWindow):

    def __init__(self, parent: QObject = None):
        super().__init__(parent=parent)
        self.__tabWidget = QTabWidget()
        self.setCentralWidget(self.__tabWidget)
        self.checkClosable()
        self.__tabWidget.setMovable(True)
        self.__tabWidget.tabCloseRequested.connect(self.removeTab)
        self.__btnAddTab = None
        self.addTab()

        if len(sys.argv) > 1:
            if len(sys.argv) > 2:
                try:
                    # self.__tabWidget.widget(0).setStep(int(sys.argv[2]))
                    index = convertStrToIndex(sys.argv[2])
                    print(index)
                    self.__tabWidget.widget(0).setStart(index[0])
                    self.__tabWidget.widget(0).setStop(index[1])
                    self.__tabWidget.widget(0).setStep(index[2])
                except Exception as e:
                    print("index could not be parsed:", e)
            self.__tabWidget.widget(0).setPath(sys.argv[1])

    def addTab(self):
        if self.__tabWidget.count() > 0:
            self.__tabWidget.removeTab(self.__tabWidget.count() - 1)
            self.__btnAddTab = None

        self.__tabWidget.addTab(MainView(), "No file loaded")
        self.__tabWidget.setCurrentIndex(self.__tabWidget.count() - 1)
        self.__tabWidget.widget(self.__tabWidget.count() - 1
                                ).pathChanged.connect(self.changeTabTitle)
        self.checkClosable()

        self.__tabWidget.addTab(QWidget(), "")
        self.__btnAddTab = QToolButton()
        self.__btnAddTab.setText("+")
        self.__btnAddTab.clicked.connect(self.addTab)
        self.__tabWidget.setTabEnabled(self.__tabWidget.count() - 1, False)
        self.__tabWidget.tabBar().setTabButton(
            self.__tabWidget.count() - 1, QTabBar.RightSide, self.__btnAddTab)

    def removeTab(self, index: int):
        self.__tabWidget.removeTab(index)
        if index > 0:
            self.__tabWidget.setCurrentIndex(index - 1)
        else:
            self.__tabWidget.setCurrentIndex(0)
        self.checkClosable()

    def checkClosable(self):
        self.__tabWidget.setTabsClosable(self.__tabWidget.count() > 1)

    def changeTabTitle(self, widget: QWidget, title: str):
        i = self.__tabWidget.indexOf(widget)
        if title is None:
            title = "No file loaded"
        else:
            title = os.path.basename(os.path.normpath(title))
        if i >= 0:
            self.__tabWidget.setTabText(i, title)

    def exportAll(self, path: str, tabIndex: int = 0):
        viewer = self.__tabWidget.widget(tabIndex)
        viewer.reload()
        viewer.exportAll(path)

    def saveImages(self, fname: str, tabIndex: int = 0):
        viewer = self.__tabWidget.widget(tabIndex)
        viewer.reload()
        viewer.saveImages(fname)
