from PyQt5.QtGui import QDoubleValidator
import numpy as np
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from mdview.signal import Signal


class CellDialog(QDialog):
    
    cellChanged = Signal()

    def __init__(self, parent: QObject = None):
        super().__init__(parent=parent)
        self.__layout = QGridLayout()
        self.setLayout(self.__layout)
        self.lineEdits = np.array(
            [[QLineEdit(), QLineEdit(), QLineEdit()],
             [QLineEdit(), QLineEdit(), QLineEdit()],
             [QLineEdit(), QLineEdit(), QLineEdit()]],
            dtype=object
        )
        for ix, iy in np.ndindex(self.lineEdits.shape):
            self.lineEdits[ix, iy].setValidator(QDoubleValidator(0., 100000., 3))
            self.__layout.addWidget(self.lineEdits[ix, iy], ix, iy)

        self.btnCancel = QPushButton("Cancel")
        self.btnApply = QPushButton("Apply")
        self.__layout.addWidget(self.btnCancel, 4, 1)
        self.__layout.addWidget(self.btnApply, 4, 2)
        self.btnApply.clicked.connect(self.__apply)

    def cell(self) -> np.ndarray:
        cell = np.zeros((3, 3))
        for ix, iy in np.ndindex(self.lineEdits.shape):
            cell[ix, iy] = float(self.lineEdits[ix, iy].text())
        return cell

    def setCell(self, cell: np.ndarray):
        for ix, iy in np.ndindex(self.lineEdits.shape):
            self.lineEdits[ix, iy].setText(str(cell[ix, iy]))

    def __apply(self):
        self.cellChanged.emit(self.cell())
