from typing import List

import numpy as np
import pyqtgraph as pg
import pandas as pd
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from ase import Atoms, units
from ase.data import covalent_radii, chemical_symbols
from ase.data.colors import jmol_colors
from mdview.signal import Signal
import matplotlib.pyplot as plt

pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')


class Viewer(QWidget):

    def __init__(self, name: str, parent: QObject = None):
        super().__init__(parent=parent)
        self.setLayout(QVBoxLayout())
        lbl = QLabel(name)
        lbl.setFixedHeight(25)
        self.name = name
        self.topLayout = QHBoxLayout()
        self.topLayout.addWidget(lbl)
        self.layout().addLayout(self.topLayout)

    def update(self, trajectory: List, currentIndex: int):
        pass

    def markIndex(self, index: int):
        pass


class AtomsPaintWidget(QWidget):

    def __init__(self, parent: QObject = None):
        super().__init__(parent=parent)
        self.atoms = None
        self.index = 0

    def paintEvent(self, event):
        if self.atoms is not None and self.index < len(self.atoms):
            positions, radii, colors = [], [], []
            for a in self.atoms[self.index]:
                positions.append([a.x, a.y, a.z])
                radii.append(covalent_radii[a.number])

            positions, radii = np.array(positions), np.array(radii)
            for i in range(3):
                positions[:, i] -= np.min(positions[:, i])
                positions[:, i] += np.max(radii)
            scale = np.min(
                [self.width() / (np.max(positions[:, 0]) + np.max(radii)),
                 self.height() / (np.max(positions[:, 1]) + np.max(radii))]
            )

            paint = QPainter()
            paint.begin(self)
            paint.setRenderHint(QPainter.Antialiasing)
            paint.setBrush(Qt.white)
            paint.drawRect(event.rect())
            for i in range(len(positions)):
                rad = scale * radii[i]
                center = QPoint(scale * positions[i, 0],
                                scale * positions[i, 1])
                c = QColor(
                    *[int(x) for x in
                      jmol_colors[self.atoms[self.index][i].number] * 255]
                )
                c.setAlpha(255 * positions[i, -1] / np.max(positions[:, -1]))
                paint.setPen(Qt.black)
                paint.setBrush(c)
                paint.drawEllipse(center, rad, rad)
            paint.end()


class DrawAtomsViewer(Viewer):

    def __init__(self, parent=None):
        super().__init__("Atoms", parent=parent)
        self.__slider = QSlider()
        self.__slider.setOrientation(Qt.Horizontal)
        self.__slider.valueChanged.connect(self.__sliderValueChanged)
        self.__sliderMaxValue = 0
        self.__lblSlider = QLabel("0")
        self.__btnBackward = QPushButton("<")
        self.__btnForward = QPushButton(">")
        style = "QPushButton { min-width: 2em; max-width: 2em; }"
        self.__btnBackward.setStyleSheet(style)
        self.__btnForward.setStyleSheet(style)
        self.__btnBackward.clicked.connect(self.__backward)
        self.__btnForward.clicked.connect(self.__forward)
        self.__topWidget = QWidget()
        self.__topWidget.setFixedHeight(25)
        self.__topWidget.setLayout(QHBoxLayout())
        self.__topWidget.layout().setContentsMargins(0, 0, 0, 0)
        self.__topWidget.layout().addWidget(self.__slider)
        self.__topWidget.layout().addWidget(self.__lblSlider)
        self.__topWidget.layout().addWidget(self.__btnBackward)
        self.__topWidget.layout().addWidget(self.__btnForward)
        self.layout().addWidget(self.__topWidget)
        self.__drawWidget = AtomsPaintWidget()
        self.layout().addWidget(self.__drawWidget)
        self.sliderValueChanged = Signal()

    def update(self, trajectory: List, currentIndex: int):
        self.__slider.setRange(0, len(trajectory) - 1)
        self.__drawWidget.atoms = trajectory
        self.__drawWidget.index = currentIndex
        self.__drawWidget.repaint()
        self.markIndex(currentIndex)

    def __sliderValueChanged(self, value: int):
        self.sliderValueChanged.emit(value)

    def __backward(self):
        if self.__slider.value() > self.__slider.minimum():
            self.__slider.setValue(self.__slider.value() - 1)

    def __forward(self):
        if self.__slider.value() < self.__slider.maximum():
            self.__slider.setValue(self.__slider.value() + 1)

    def markIndex(self, value: int):
        self.__drawWidget.index = value
        self.__lblSlider.setText(
            str(value) + "/" + str(len(self.__drawWidget.atoms) - 1))
        self.__drawWidget.repaint()


class SimplePlotViewer(Viewer):

    def __init__(self, name: str, unit: str = None, parent: QObject = None):
        super().__init__(name, parent=parent)
        self.plotWidget = pg.PlotWidget()
        self.plotWidget.getPlotItem().setMouseEnabled(False, False)
        self.layout().addWidget(self.plotWidget)
        self.btnExport = QPushButton("Export")
        self.btnExport.setFixedWidth(75)
        self.btnExport.clicked.connect(lambda _: self.export())
        self.topLayout.addWidget(self.btnExport)
        self.indexLine, self.curveArrow, self.textItem = None, None, None
        self._x, self._y = None, None
        self.unit = unit
        label = self.name
        if unit is not None:
            label += " / " + self.unit
        self.plotWidget.getPlotItem().setLabel("left", label)

    def plotData(self, x: np.ndarray, y: np.ndarray, currentIndex: int):
        keep = ~np.isnan(y)
        x, y = x[keep], y[keep]
        self._x, self._y = x, y
        self.plotWidget.getPlotItem().clear()
        self.plotWidget.getPlotItem().plot(x, y, pen="k")
        self.curveArrow = pg.CurveArrow(
            self.plotWidget.getPlotItem().curves[0])
        self.plotWidget.addItem(self.curveArrow)
        self.indexLine = self.plotWidget.getPlotItem().addLine(x=0)
        self.textItem = pg.TextItem()
        self.plotWidget.addItem(self.textItem)
        self.markIndex(currentIndex)
        self.plotWidget.getViewBox().autoRange()

    def markIndex(self, index: int):
        if self.indexLine is not None:
            self.indexLine.setValue(index)
        if (self.curveArrow is not None and self._x is not None and
                index in self._x):
            index_arrow = list(self._x).index(index)
            self.curveArrow.setIndex(index_arrow)
        if self.textItem is not None:
            self.textItem.setText("%.2g" % (self.curveArrow.pos().y()))
            self.textItem.setPos(self.curveArrow.pos())

    def export(self, path: str = ""):
        if self._x is not None and self._y is not None:
            if path == "":
                path = QFileDialog.getSaveFileName(self, "Save data")[0]

            if isinstance(path, str) and path != "":
                df = pd.DataFrame()
                df["Step"] = self._x
                df[self.name] = self._y

                if path.endswith(".csv"):
                    df.to_csv(path)
                elif path.endswith(".hdf5") or path.endswith(".hdf"):
                    df.to_hdf(path)
                elif path.endswith(".pdf") or path.endswith(".png"):
                    fig = plt.figure(figsize=(5, 4))
                    plt.plot(df["Step"], df[self.name])
                    plt.xlabel("step")
                    plt.ylabel(self.name + " / " + self.unit)
                    plt.savefig(path, bbox_inches="tight", dpi=300)


class TemperatureViewer(SimplePlotViewer):

    overwriteData = None

    def __init__(self, parent: QObject = None):
        super().__init__("Temperature", "K", parent=parent)
        self.__trajectory = None

    def update(self, trajectory: List, currentIndex: int):
        self.__trajectory = trajectory
        if self.overwriteData is None:
            data = np.array([a.get_temperature() for a in trajectory])
            self.plotData(np.arange(len(data)), data, currentIndex)

            data = self.__getSingleAtomTypeTemperature(trajectory)
            for key in data:
                for i in range(len(chemical_symbols)):
                    if chemical_symbols[i] == key:
                        c = QColor(*[int(x) for x in jmol_colors[i] * 255])
                        pen = QPen(c)
                        # pen.setWidthF(0.5)
                        break
                self.plotWidget.getPlotItem().plot(
                    np.arange(len(data[key])), data[key], pen=pen)
            self.plotWidget.getViewBox().autoRange()
        else:
            self.plotData(np.arange(len(self.overwriteData)),
                          self.overwriteData,
                          currentIndex)

    def __getSingleAtomTypeTemperature(self, trajectory: List):
        temp = {}
        try:
            for atoms in trajectory:
                symb, m = atoms.get_chemical_symbols(), atoms.get_masses()
                vel = np.linalg.norm(atoms.get_velocities(), axis=1)
                energies = {}

                for i in range(len(atoms)):
                    if symb[i] not in energies:
                        energies[symb[i]] = []
                    if vel[i] > 1e-9:
                        energies[symb[i]].append(0.5 * m[i] * vel[i]**2)

                for key in energies:
                    if key not in temp:
                        temp[key] = []
                    temp[key].append(2 * np.sum(energies[key]) /
                                     (len(energies[key]) * 3 * units.kB))
        except Exception as e:
            print(e)
        return temp

    def export(self, path: str = ""):
        if (self._x is not None and self._y is not None and
                self.__trajectory is not None):
            if path == "":
                path = QFileDialog.getSaveFileName(self, "Save data")[0]

            print(path)

            if isinstance(path, str) and path != "":
                df = pd.DataFrame()
                df["Step"] = self._x
                df[self.name] = self._y
                data = self.__getSingleAtomTypeTemperature(self.__trajectory)
                for key in data:
                    df[key] = data[key]

                if path.endswith(".csv"):
                    df.to_csv(path)
                elif path.endswith(".hdf5") or path.endswith(".hdf"):
                    df.to_hdf(path)
                elif path.endswith(".pdf") or path.endswith(".png"):
                    fig = plt.figure(figsize=(6, 4))
                    for key in data:
                        for i in range(len(chemical_symbols)):
                            if chemical_symbols[i] == key:
                                c = tuple([float(x) for x in jmol_colors[i]])
                                break
                        plt.plot(df["Step"], df[key], ls="--", color=c,
                                 alpha=0.25, label=key)
                    plt.plot(df["Step"], df[self.name], "k", label="total")
                    plt.xlabel("step")
                    plt.ylabel(self.name + " / " + self.unit)
                    plt.legend()
                    plt.savefig(path, bbox_inches="tight", dpi=300)


class StressViewer(SimplePlotViewer):

    def __init__(self, parent: QObject = None):
        super().__init__("Stress", "Pa", parent=parent)

    def update(self, trajectory: List, currentIndex: int):
        self.plotWidget.getPlotItem().clear()
        try:
            data = np.array([a.get_stress() for a in trajectory])
        except Exception as e:
            print(e)
            return

        # for i in range(data.shape[1]):
        #     self.plotWidget.getPlotItem().plot(
        #         np.arange(data.shape[0]), data[:, i], pen="k")

        self.plotData(np.arange(data.shape[0]),
                      (data[:, 0] + data[:, 1] + data[:, 2]) / 3,
                      currentIndex)


class TotalEnergyViewer(SimplePlotViewer):

    overwriteKineticEnergy = None

    def __init__(self, parent: QObject = None):
        super().__init__("Total energy", "eV", parent=parent)

    def update(self, trajectory: List, currentIndex: int):
        try:
            data = np.array([a.get_total_energy() for a in trajectory])
            self.plotData(np.arange(len(data)), data, currentIndex)
        except Exception:
            pass


class KineticEnergyViewer(SimplePlotViewer):

    overwriteData = None

    def __init__(self, parent: QObject = None):
        super().__init__("Kinetic energy", "eV", parent=parent)
        self.plotWidget.getPlotItem().setLabel("left", "kinetic energy / eV")

    def update(self, trajectory: List, currentIndex: int):
        try:
            if self.overwriteData is None:
                data = np.array([a.get_kinetic_energy() for a in trajectory])
            else:
                data = self.overwriteData
            self.plotData(np.arange(len(data)), data, currentIndex)
        except Exception:
            pass


class PotentialEnergyViewer(SimplePlotViewer):

    def __init__(self, parent: QObject = None):
        super().__init__("Potential energy", "eV", parent=parent)

    def update(self, trajectory: List, currentIndex: int):
        try:
            data = np.array([a.get_potential_energy() for a in trajectory])
            self.plotData(np.arange(len(data)), data, currentIndex)
        except Exception:
            pass


class CellViewer(Viewer):

    def __init__(self, parent: QObject = None):
        super().__init__("Cell information", parent=parent)
        self.__plotLayout = QHBoxLayout()
        self.plotWidgetV = pg.PlotWidget()
        self.plotWidgetA = pg.PlotWidget()
        self.plotWidgetB = pg.PlotWidget()
        self.plotWidgetC = pg.PlotWidget()
        self.plotWidgetV.getPlotItem().setLabel("left", "volume / A^3")
        self.plotWidgetA.getPlotItem().setLabel("left", "a / A")
        self.plotWidgetB.getPlotItem().setLabel("left", "b / A")
        self.plotWidgetC.getPlotItem().setLabel("left", "c / A")
        for w in [self.plotWidgetV, self.plotWidgetA,
                  self.plotWidgetB, self.plotWidgetC]:
            w.getPlotItem().setMouseEnabled(False, False)
            self.__plotLayout.addWidget(w)
        self.layout().addLayout(self.__plotLayout)

    def update(self, trajectory: List, currentIndex: int):
        try:
            V = np.array([a.get_volume() for a in trajectory])
            cells = [a.get_cell() for a in trajectory]
            a = np.array([np.linalg.norm(c[0, :]) for c in cells])
            b = np.array([np.linalg.norm(c[1, :]) for c in cells])
            c = np.array([np.linalg.norm(c[2, :]) for c in cells])
            for w in [self.plotWidgetV, self.plotWidgetA,
                      self.plotWidgetB, self.plotWidgetC]:
                w.getPlotItem().clear()
            self.plotWidgetV.getPlotItem().plot(np.arange(len(V)), V, pen="k")
            self.plotWidgetA.getPlotItem().plot(np.arange(len(a)), a, pen="k")
            self.plotWidgetB.getPlotItem().plot(np.arange(len(b)), b, pen="k")
            self.plotWidgetC.getPlotItem().plot(np.arange(len(c)), c, pen="k")
        except Exception:
            pass


class TemperatureProfileViewer(Viewer):

    def __init__(self, parent: QObject = None):
        super().__init__("Temperature profile", parent=parent)
        self.__plotLayout = QHBoxLayout()
        self.plotWidgetA = pg.PlotWidget()
        self.plotWidgetB = pg.PlotWidget()
        self.plotWidgetC = pg.PlotWidget()
        self.plotWidgetA.getPlotItem().setLabel("bottom", "a")
        self.plotWidgetB.getPlotItem().setLabel("bottom", "b")
        self.plotWidgetC.getPlotItem().setLabel("bottom", "c")
        self.plotWidgetA.getPlotItem().setLabel("left", "T / K")
        self.plotWidgetB.getPlotItem().setLabel("left", "T / K")
        self.plotWidgetC.getPlotItem().setLabel("left", "T / K")
        for w in [self.plotWidgetA, self.plotWidgetB, self.plotWidgetC]:
            w.getPlotItem().setMouseEnabled(False, False)
            self.__plotLayout.addWidget(w)
        self.layout().addLayout(self.__plotLayout)
        self.__trajectory = None

    def update(self, trajectory: List, currentIndex: int):
        self.__trajectory = trajectory
        self.markIndex(currentIndex)

    def markIndex(self, index: int):
        for w in [self.plotWidgetA, self.plotWidgetB, self.plotWidgetC]:
            w.getPlotItem().clear()

        if self.__trajectory is not None:
            atoms = self.__trajectory[index]
            for i, w in enumerate(
                    [self.plotWidgetA, self.plotWidgetB, self.plotWidgetC]):
                try:
                    p = self.__calculateProfile(atoms, i)
                    w.getPlotItem().plot(np.arange(len(p)), p, pen="k")
                except Exception as e:
                    print(e)

    def __calculateProfile(self, atoms: Atoms, direction: int, split: int = 10):
        pos, vel = atoms.get_scaled_positions(), atoms.get_velocities()
        vel = np.linalg.norm(vel, axis=1)
        m = atoms.get_masses()
        cell = atoms.get_cell()
        energies = []

        for i in range(split):
            energies_temp = []
            for j in range(len(atoms)):
                p = pos[j, direction]
                if p >= i / split and p < (i + 1) / split:
                    energies_temp.append(0.5 * m[j] * vel[j]**2)
            energies.append(energies_temp)

        return [2 * np.sum(e) / (len(e) * 3 * units.kB) for e in energies]
