import os
import re
from io import StringIO

import numpy as np
from ase import calculators, io, visualize
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import mdview.cp2k as cp2k
from mdview.helperwidgets import *
from mdview.util import convertIndexToStr
from mdview.viewers import *
from mdview.celldialog import CellDialog


class MainView(QWidget):

    def __init__(self, parent: QObject = None):
        super().__init__(parent=parent)
        self.indexChanged = Signal()
        self.currentIndex = 0

        self.__overallLayout = QVBoxLayout()
        self.setLayout(self.__overallLayout)
        self.__scrollableWidget = QWidget()
        self.__scrollableWidget.setFixedHeight(1400)
        self.__scrollArea = QScrollArea()
        self.__scrollArea.setWidgetResizable(True)
        self.__txtPath = PathLineEdit()
        self.__txtPath.enterPressed.connect(self.reload)
        self.__lblReadStart = QLabel("Start:")
        self.__sbReadStart = QSpinBox()
        self.__sbReadStart.setMinimum(0)
        self.__sbReadStart.setMaximum(1000000)
        self.__sbReadStart.setSingleStep(1)
        self.__sbReadStart.setValue(0)
        self.__lblReadStop = QLabel("Stop:")
        self.__sbReadStop = QSpinBox()
        self.__sbReadStop.setSingleStep(1)
        self.__sbReadStop.setMaximum(1000000)
        self.__sbReadStop.setValue(0)
        self.__lblReadStep = QLabel("Stride:")
        self.__sbReadStep = QSpinBox()
        self.__sbReadStep.setRange(1, 10000)
        self.__sbReadStep.setSingleStep(1)
        self.__sbReadStep.setValue(1)
        self.__btnSelectFile = QPushButton("Select file")
        self.__btnSetCell = QPushButton("Set cell dimensions")
        self.__btnWrap = QPushButton("Wrap atoms")
        self.__btnOpenASE = QPushButton("Open in ASE GUI")
        self.__btnSaveSnapshot = QPushButton("Save snapshot")
        self.__btnReload = QPushButton("Reload")
        self.__topWidget = QWidget()
        self.__topWidget.setLayout(QHBoxLayout())
        self.__topWidget.layout().addWidget(self.__btnSelectFile)
        self.__topWidget.layout().addWidget(self.__txtPath)
        self.__topWidget.layout().addWidget(self.__lblReadStart)
        self.__topWidget.layout().addWidget(self.__sbReadStart)
        self.__topWidget.layout().addWidget(self.__lblReadStop)
        self.__topWidget.layout().addWidget(self.__sbReadStop)
        self.__topWidget.layout().addWidget(self.__lblReadStep)
        self.__topWidget.layout().addWidget(self.__sbReadStep)
        self.__topWidget.layout().addWidget(self.__btnSetCell)
        self.__topWidget.layout().addWidget(self.__btnWrap)
        self.__topWidget.layout().addWidget(self.__btnOpenASE)
        self.__topWidget.layout().addWidget(self.__btnSaveSnapshot)
        self.__topWidget.layout().addWidget(self.__btnReload)
        self.__overallLayout.addWidget(self.__topWidget)
        self.__overallLayout.addWidget(self.__scrollArea)

        self.__scrollLayout = QGridLayout()
        self.__scrollableWidget.setLayout(self.__scrollLayout)
        self.__btnSelectFile.clicked.connect(self.setPath)
        self.__btnSetCell.clicked.connect(self.setCell)
        self.__btnWrap.clicked.connect(self.wrap)
        self.__btnOpenASE.clicked.connect(self.openASE)
        self.__btnSaveSnapshot.clicked.connect(self.saveSnapshot)
        self.__btnReload.clicked.connect(self.reload)

        self.__atomsViewer = DrawAtomsViewer()
        self.__atomsViewer.sliderValueChanged.connect(self.indexChanged.emit)
        self.__scrollLayout.addWidget(self.__atomsViewer, 0, 0)
        self.__temperatureViewer = TemperatureViewer()
        self.__scrollLayout.addWidget(self.__temperatureViewer, 0, 1)
        self.__pressureViewer = StressViewer()
        self.__scrollLayout.addWidget(self.__pressureViewer, 0, 2)
        self.__totalEnergyViewer = TotalEnergyViewer()
        self.__scrollLayout.addWidget(self.__totalEnergyViewer, 1, 0)
        self.__kineticEnergyViewer = KineticEnergyViewer()
        self.__scrollLayout.addWidget(self.__kineticEnergyViewer, 1, 1)
        self.__potentialEnergyViewer = PotentialEnergyViewer()
        self.__scrollLayout.addWidget(self.__potentialEnergyViewer, 1, 2)
        self.__temperatureProfileViewer = TemperatureProfileViewer()
        self.__scrollLayout.addWidget(self.__temperatureProfileViewer,
                                      2, 0, 1, 3)
        self.__cellViewer = CellViewer()
        self.__scrollLayout.addWidget(self.__cellViewer, 3, 0, 1, 3)

        self.__viewers = [self.__atomsViewer, self.__temperatureViewer,
                          self.__pressureViewer, self.__totalEnergyViewer,
                          self.__kineticEnergyViewer,
                          self.__potentialEnergyViewer,
                          self.__temperatureProfileViewer,
                          self.__cellViewer]
        for v in self.__viewers:
            self.indexChanged.connect(v.markIndex)
        self.__scrollArea.setWidget(self.__scrollableWidget)
        self.__traj = None
        self.indexChanged.connect(self.updateIndex)

        self.pathChanged = Signal()
        self.__path = None

    @property
    def path(self) -> str:
        return self.__path

    @path.setter
    def path(self, path: str):
        self.__path = path
        self.pathChanged.emit(self, path)

    def setPath(self, path: str = ""):
        if path == "" or not isinstance(path, str):
            path = QFileDialog.getOpenFileName(self, "Open trajectory file")[0]

        if path != "":
            self.__txtPath.setText(path)
            # self.reload()

    def setCell(self):
        dialog = CellDialog(self)
        if self.__traj is not None and len(self.__traj) > 0:
            dialog.setCell(self.__traj[0].get_cell())
        else:
            dialog.setCell(np.identity(3))
        dialog.cellChanged.connect(self.updateCell)
        dialog.show()

    def updateCell(self, cell: np.ndarray):
        for a in self.__traj:
            a.set_cell(cell)
            a.set_pbc(True)
        self.updateViewers()

    def wrap(self):
        for a in self.__traj:
            a.wrap()
        self.updateViewers()

    def setStart(self, start: int):
        self.__sbReadStart.setValue(start)

    def setStop(self, stop: int):
        self.__sbReadStop.setValue(stop)

    def setStep(self, step: int):
        self.__sbReadStep.setValue(step)

    def openASE(self):
        if self.__traj is not None:
            visualize.view(self.__traj)

    def saveSnapshot(self, path: str = ""):
        if self.__traj is not None and self.currentIndex is not None:
            if path == "" or not isinstance(path, str):
                path = QFileDialog.getSaveFileName(self, "Open snapshot")[0]

            if path != "":
                io.write(path, self.__traj[self.currentIndex])

    def updateIndex(self, index: int):
        self.currentIndex = index

    def reload(self):
        self.setEnabled(False)
        QApplication.processEvents()

        self.__temperatureViewer.overwriteData = None
        self.__kineticEnergyViewer.overwriteData = None
        self.__totalEnergyViewer.overwriteKineticEnergy = None

        path = self.__txtPath.text()
        try:
            index = (int(self.__sbReadStart.value()),
                     int(self.__sbReadStop.value()),
                     int(self.__sbReadStep.value()))
            self.__traj = io.read(path, index=convertIndexToStr(index))
            if path.endswith(".xyz"):
                self.__traj, add_data = cp2k.read(path, self.__traj, index)
                if "temperature" in add_data:
                    self.__temperatureViewer.overwriteData = add_data["temperature"]
                if "kinetic_energy" in add_data:
                    self.__kineticEnergyViewer.overwriteData = add_data["kinetic_energy"]
                    self.__totalEnergyViewer.overwriteKineticEnergy = add_data["kinetic_energy"]
            self.path = path
        except Exception as e:
            msg = QMessageBox(self)
            msg.setWindowTitle("File could not be loaded")
            msg.setText(str(e))
            msg.exec_()
            self.setEnabled(True)
            self.path = None
            return

        self.updateViewers()

    def updateViewers(self):
        if self.__traj is not None and len(self.__traj) > 0:
            for v in self.__viewers:
                v.update(self.__traj, self.currentIndex)
        self.setEnabled(True)

    def exportAll(self, path: str):
        for v in self.__viewers:
            if hasattr(v, "export"):
                print("exporting", v.name)
                v.export(os.path.join(path, v.name + ".pdf"))
                v.export(os.path.join(path, v.name + ".png"))

    def saveImages(self, fname: str):
        try:
            io.write(fname, self.__traj)
        except Exception as e:
            print(e)
