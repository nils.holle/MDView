from typing import Tuple


def convertIndexToStr(index: Tuple):
    out = ""
    for i in index:
        if i != 0:
            out += "%d" % (i)
        out += ":"
    if len(index) > 0:
        out = out[:-1]
    return out

def convertStrToIndex(text: str):
    index = text.split(":")
    index = [int(i) if i != "" else 0 for i in index]
    if len(index) < 3:
        index.append(1)
    if index[1] == 0:
        index[1] = -1
    if index[2] == 0:
        index[2] = 1
    return tuple(index)
