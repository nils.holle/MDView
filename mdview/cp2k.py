import re
import os
from typing import List, Tuple

import numpy as np
import pandas as pd

from ase import io, units
from ase.calculators.cp2k import parse_input
from ase.calculators.singlepoint import SinglePointCalculator
from mdview.util import convertIndexToStr


def readEner(path: str):
    with open(path) as f:
        enerHeaders = re.sub(r"\s{4,}", ";", f.readline()).split(";")[1:]
    enerHeaders = [e.rstrip() for e in enerHeaders]
    enerData = np.genfromtxt(path)
    frame = pd.DataFrame(
        data=enerData,
        columns=enerHeaders
    )
    return frame


def read(path: str, traj: List, index: Tuple) -> List:
    try:
        # base_path = os.path.splitext(path)[0]
        vel = io.read(path.replace("-pos-1.xyz", "-vel-1.xyz"),
                      index=convertIndexToStr(index))
        for i in range(len(traj)):
            # the 222.9025 works, but need a better solution for this
            traj[i].set_velocities(vel[i].get_positions() * 222.9025)
    except Exception as e:
        print("Error during during reading of velocities:", e)

    try:
        inp_path = None
        if os.path.isfile(path.replace("-pos-1.xyz", "") + ".inp"):
            inp_path = path.replace("-pos-1.xyz", "") + ".inp"
        else:
            for f in os.listdir(os.path.dirname(os.path.abspath(path))):
                if f.endswith(".inp"):
                    inp_path = os.path.join(
                        os.path.dirname(os.path.abspath(path)), f)
                    break
            else:
                raise Exception("Input file not available.")

        if inp_path is not None:
            with open(inp_path) as f:
                inp = parse_input(f.read())
            cell = None
            for s in inp.subsections:
                if s.name == "FORCE_EVAL":
                    for ss in s.subsections:
                        if ss.name == "SUBSYS":
                            for sss in ss.subsections:
                                if sss.name == "CELL":
                                    cell = sss.keywords

            if cell is None:
                raise Exception("Cell not available in input file.")
            else:
                match_number = re.compile(
                    "-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[+-]?\ *[0-9]+)?")
                cell = re.findall(
                    match_number, " ".join([k for k in cell]))
                cell = np.array([float(n) for n in cell])
                if len(cell) > 3:
                    cell = cell.reshape((3, 3))

                for i in range(len(traj)):
                    traj[i].set_cell(cell)
                    traj[i].set_pbc(True)
    except Exception as e:
        print("Error during during reading of cell:", e)

    for a in traj:
        a.wrap()

    add_data = {}

    try:
        data = readEner(path.replace("-pos-1.xyz", "-1.ener"))
        indices = [int(i) for i in data["Step Nr."]]
        energy = np.array(data["Pot.[a.u.]"])
        add_data["temperature"] = np.array(data["Temp[K]"])
        add_data["kinetic_energy"] = np.array(data["Kin.[a.u.]"])

        energy *= units.Ry
        for i in range(len(traj)):
            calc = SinglePointCalculator(traj[i])
            traj[i].set_calculator(calc)
            step = index[0] + i * index[2] + indices[0]
            if step in indices:
                i_energy = indices.index(step)
                calc.results["energy"] = energy[i_energy]
            else:
                calc.results["energy"] = np.nan
    except Exception as e:
        print("Error during during reading of energy:", e)

    return traj, add_data
