#!/bin/python3

# -*- coding: utf-8 -*-
import os
import re
import sys

sys.path = sorted(sys.path)

if __name__ == '__main__':
    import mdview.launch as launch
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(launch.saveImages())
