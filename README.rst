MDView
======

Tool for fast visualization of MD simulations
---------------------------------------------

![alt text](doc/Screenshot.png "Screenshot")

Installation:
```
python3 -m pip install --user --upgrade -r requirements.txt
```
or
```
python -m pip install --user --upgrade -r requirements.txt
```
(depending on your Python installation)

Run:
```
python3 MDView.py
```
or
```
python MDView.py
```
This may optionally be followed by a file path to open (.traj, .xyz, ...;
see ASE documentation for supported file types) and an integer indicating the
step size between two images to be read (read every / every second /
every third / ... image).
